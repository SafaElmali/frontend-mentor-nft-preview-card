# Frontend Mentor - NFT preview card component solution

This is a solution to the [NFT preview card component challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/nft-preview-card-component-SbdUL_w0U). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

## Table of contents

- [Frontend Mentor - NFT preview card component solution](#frontend-mentor---nft-preview-card-component-solution)
  - [Table of contents](#table-of-contents)
  - [Overview](#overview)
    - [The challenge](#the-challenge)
    - [Screenshot](#screenshot)
    - [Links](#links)
  - [My process](#my-process)
    - [Built with](#built-with)
  - [Author](#author)

## Overview

### The challenge

Users should be able to:

- View the optimal layout for the app depending on their device's screen size
- See hover states for all interactive elements on the page
- Generate a new piece of advice by clicking the dice icon

### Screenshot

![](./screenshot/screenshot.png)

### Links

- [Solution URL](https://gitlab.com/SafaElmali/frontend-mentor-nft-preview-card)
- [Live Site URL](https://frontend-mentor-nft-preview-card-phi.vercel.app/)

## My process

### Built with

- Flexbox
- [React](https://reactjs.org/) - JS library
- [Styled Components](https://styled-components.com/) - For styles

## Author

- Website - [Safa Elmali](https://safaelmali.com/)
- Frontend Mentor - [@SafaElmali](https://www.frontendmentor.io/profile/SafaElmali)
- Twitter - [@SafaElmali](https://twitter.com/SafaElmali)
- Github - [@SafaElmali](https://github.com/SafaElmali)
