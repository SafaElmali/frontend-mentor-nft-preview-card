import { createGlobalStyle } from "styled-components";

const Styles = createGlobalStyle`
    @import url('https://fonts.googleapis.com/css2?family=Outfit:wght@300;400;600&display=swap');

    :root{

    /* COLORS */

    --color-soft-blue: hsl(215, 51%, 70%);
    --color-cyan: hsl(178, 100%, 50%);
    --color-very-dark-blue-main:hsl(217, 54%, 11%);
    --color-very-dark-blue-card:hsl(216, 50%, 16%);
    --color-very-dark-blue-line:hsl(215, 32%, 27%);
    --color-white:hsl(0, 0%, 100%);

    /* WEIGHTS */
    --font-weight-300: 300;
    --font-weight-400: 400;
    --font-weight-600: 600;
    }

    html{
        box-sizing:border-box;
    }

    *,*::after,*::before{
        box-sizing: inherit;
    }

    body {
        margin:0;
        padding:0;
        font-size: 18px;
        font-family: 'Outfit', sans-serif;
    }
`;

export default Styles;
