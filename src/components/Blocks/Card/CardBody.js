import styled from "styled-components/macro";
import { HFlex } from "../../Atoms/Layouts/Flex";
import Clock from "../../../assets/icon-clock.svg";
import Ethereum from "../../../assets/icon-ethereum.svg";

const CardBody = () => (
  <>
    <CardTitle>Equilibrium #3429</CardTitle>
    <CardDescription>
      Our Equilibrium collection promotes balance and calm.
    </CardDescription>
    <PriceAndDateContainer justify="between">
      <PriceContainer gap="7px" align="center" justify="center">
        <PriceIcon src={Ethereum} alt="etherium icon" />
        <Price>0.041 ETH</Price>
      </PriceContainer>
      <DateContainer gap="10px" align="center" justify="center">
        <ClockIcon src={Clock} alt="clock icon" />
        <Date>3 days left</Date>
      </DateContainer>
    </PriceAndDateContainer>
  </>
);

export default CardBody;

const CardTitle = styled.h5`
  color: var(--color-white);
  margin-bottom: 12px;
  cursor: pointer;
  &:hover {
    color: var(--color-cyan);
  }
`;

const CardDescription = styled.p`
  margin: 0;
  color: var(--color-soft-blue);
  line-height: 26px;
  font-size: 18px;
  height: auto;
  font-weight: var(--font-weight-300);
`;

const PriceContainer = styled(HFlex)``;

const PriceIcon = styled.img``;

const Price = styled.p`
  color: var(--color-cyan);
  font-weight: var(--font-weight-600);
`;

const PriceAndDateContainer = styled(HFlex)`
  font-size: 15px;
`;

const DateContainer = styled(HFlex)`
  color: var(--color-soft-blue);
  font-weight: var(--font-weight-400);
`;

const ClockIcon = styled.img``;

const Date = styled.time``;
