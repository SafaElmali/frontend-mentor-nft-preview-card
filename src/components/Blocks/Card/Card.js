import { useEffect } from "react";
import styled from "styled-components/macro";
import { HFlex, VFlex } from "../../Atoms/Layouts/Flex";
import CardBody from "./CardBody";
import CardFooter from "./CardFooter";
import CardHeader from "./CardHeader";

// Reference: https://www.frontendmentor.io/challenges/nft-preview-card-component-SbdUL_w0U
const Card = () => {
  useEffect(() => {
    document.title = "Frontend Mentor | NFT preview card component";
  }, []);

  return (
    <>
      <Container align="center" justify="center">
        <StyledCard>
          <CardHeader />
          <CardBody />
          <Divider />
          <CardFooter />
        </StyledCard>
      </Container>
    </>
  );
};

export default Card;

const Divider = styled.hr`
  display: block;
  height: 1px;
  border: 0;
  border-top: 1px solid var(--color-very-dark-blue-line);
  background-color: var(--color-very-dark-blue-line);
  margin-top: 8px;
  margin-bottom: 16px;
  width: 100%;
  padding: 0;
`;

const Container = styled(HFlex)`
  background-color: var(--color-very-dark-blue-main);
  min-height: 100vh;
`;

const StyledCard = styled(VFlex)`
  margin: 24px 24px;
  padding: 24px;
  background-color: var(--color-very-dark-blue-card);
  border-radius: 15px;
  max-width: 350px;
`;
