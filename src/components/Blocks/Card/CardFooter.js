import styled from "styled-components/macro";
import { HFlex } from "../../Atoms/Layouts/Flex";
import Avatar from "../../../assets/image-avatar.png";

const CardFooter = () => (
  <UserDetailContainer align="center">
    <UserAvatar src={Avatar} alt="user avatar" />
    <Detail>
      Creation Of <UserName>Jules Wyvern</UserName>
    </Detail>
  </UserDetailContainer>
);

export default CardFooter;

const Detail = styled.div``;

const UserDetailContainer = styled(HFlex)`
  color: var(--color-soft-blue);
  font-size: 15px;
  gap: 16px;
`;

const UserAvatar = styled.img`
  max-width: 33px;
  border-width: 2px;
  border-style: solid;
  border-radius: 50%;
  border-color: var(--color-white);
`;

const UserName = styled.span`
  color: var(--color-white);
  &:hover {
    cursor: pointer;
    color: var(--color-cyan);
  }
`;
