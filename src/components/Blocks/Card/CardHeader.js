import styled from "styled-components/macro";
import { HFlex } from "../../Atoms/Layouts/Flex";
import Equilibrium from "../../../assets/image-equilibrium.jpg";
import IconView from "../../../assets/icon-view.svg";

const CardHeader = () => (
  <CardImageContainer align="center" justify="center">
    <CardImage src={Equilibrium} alt="equilibrium image" />
    <CardImageView src={IconView} alt="icon image" />
  </CardImageContainer>
);

export default CardHeader;

const CardImageView = styled.img`
  position: absolute;
  margin-left: auto;
  margin-right: auto;
  opacity: 0;
  left: 0;
  right: 0;
  text-align: center;
  transition: all 0.3s ease-in-out;
`;

const CardImageContainer = styled(HFlex)`
  cursor: pointer;
  position: relative;
  text-align: center;
  &::after {
    content: "";
    inset: 0;
    background-color: var(--color-cyan);
    opacity: 0;
    position: absolute;
    transition: all 0.3s ease-in-out;
  }
  &:hover {
    &::after {
      opacity: 0.5;
    }
    ${CardImageView} {
      opacity: 1;
      z-index: 10;
    }
  }
`;

const CardImage = styled.img`
  display: block;
  max-width: 302px;
  width: 100%;
  border-radius: 8px;
`;
