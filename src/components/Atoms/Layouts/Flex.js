import styled from "styled-components/macro";

const DIRECTIONS = {
  start: "flex-start",
  end: "flex-end",
  center: "center",
  between: "space-between",
  around: "space-around",
  evenly: "space-evenly",
};

const Flex = styled.div`
  display: flex;
  flex-direction: ${({ direction }) => DIRECTIONS[direction]};
  flex-wrap: ${({ wrap }) => (wrap ? "wrap" : "nowrap")};
  justify-content: ${({ justify }) => DIRECTIONS[justify]};
  align-items: ${({ align }) => DIRECTIONS[align]};
  align-content: ${({ alignContent }) => DIRECTIONS[alignContent]};
  gap: ${({ gap }) => gap};
`;

export const HFlex = styled(Flex)`
  flex-direction: row;
`;

export const VFlex = styled(Flex)`
  flex-direction: column;
`;
