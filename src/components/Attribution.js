import styled from "styled-components";

const Attribution = () => {
  return (
    <StyledAttribution>
      Challenge by{" "}
      <AttributionLink
        href="https://www.frontendmentor.io?ref=challenge"
        target="_blank"
      >
        Frontend Mentor.{" "}
      </AttributionLink>
      Coded by <AttributionLink href="#">T.Safa Elmali</AttributionLink>
    </StyledAttribution>
  );
};

export default Attribution;

const StyledAttribution = styled.div`
  font-size: 11px;
  text-align: center;
  color: var(--color-white);
  position: fixed;
  bottom: 20px;
  left: 0;
  right: 0;
`;

const AttributionLink = styled.a`
  color: hsl(228, 45%, 44%);
`;
